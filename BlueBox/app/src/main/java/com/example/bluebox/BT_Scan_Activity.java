/*
BT_Scan_Activity.java defines the behavior of the activity which lists speakers available to pair
with the box.

Author: Liam Helfrich (whn7g@mst.edu)

NOTE: We were unable to establish serial communication with the transmitters. The rise times
of the responses to our requests were very poor, making the signal unreadable. Thus, the list
of available speakers cannot be obtained from the transmitter and this activity is useless.

This activity is unreachable from the main app, but the code is still here for future use.
 */

package com.example.bluebox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;


/*
List items are Bluetooth devices:
 */
class BT_Device{
    public String name;
    public String address;

    public BT_Device(String name, String address){
        this.name = name;
        this.address = address;
    }
}

class BT_Device_List_Adapter extends ArrayAdapter<BT_Device>{
    /*
    Inspired by Mitch Tabian's Bluetooth Discover Devices example
    https://github.com/mitchtabian/Bluetooth---Discover-Devices/blob/master/Bluetooth-DiscoverDevices/app/src/main/java/com/example/user/bluetooth_discoverdevices/DeviceListAdapter.java

    Defines a custom device adapter for displaying Bluetooth device data to a list.
     */
    private LayoutInflater mLayoutInflater;
    private ArrayList<BT_Device> mDeviceList; //List of BT devices to display
    private int mLayoutId;
    public BT_Device_List_Adapter(@NonNull Context context, int resource, ArrayList<BT_Device> device_list) {
        super(context, resource, device_list);

        mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDeviceList = device_list;
        mLayoutId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = mLayoutInflater.inflate(mLayoutId, null);
        BT_Device device = mDeviceList.get(position);

        if(device != null){
           TextView device_name = convertView.findViewById(android.R.id.text1);
           TextView device_address = convertView.findViewById(android.R.id.text2);

           /*
           Populate the name field:
            */
           if(device_name!=null) {
               device_name.setText(device.name);
               device_name.setTextSize(20);

           }

           /*
           Populate the address field
            */
           if(device_address!=null){
               device_address.setText("0x" + device.address);
               device_address.setTextColor(Color.parseColor("#969696"));
               device_address.setTextSize(16);
           }

        }

        return convertView;
    }
}
public class BT_Scan_Activity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    /* View elements */
    private TextView list_title;
    private TextView scanning_message;
    private ListView device_list_view;
    private Button retry_button;

    /* Bluetooth handling */
    private BluetoothHandler mBtHandler;

    /* Channel data */
    private byte channel_to_use;

    private ArrayList<BT_Device> device_list;
    private BT_Device_List_Adapter device_list_adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bt_scan);

        /*
        Get view elements
         */
        list_title = findViewById(R.id.pair_list_title);
        scanning_message = findViewById(R.id.text_scanning);
        device_list_view = findViewById(R.id.device_list);
        retry_button = findViewById(R.id.retry_button);

        mBtHandler = BluetoothHandler.getInstance(getApplicationContext());

        /*
        Set the screen title based on which channel we are pairing:
         */
        Intent i = getIntent();
        channel_to_use = i.getByteExtra("channel", Box_Commands.CHANNEL_A);
        switch(channel_to_use){
            case Box_Commands.CHANNEL_A:
                list_title.setText("Pair Channel A");
                break;
            case Box_Commands.CHANNEL_B:
                list_title.setText("Pair Channel B");
                break;

        }

        retry_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Clear the list and scan again.
                 */
                device_list.clear();
                device_list_adapter = new BT_Device_List_Adapter(BT_Scan_Activity.this, android.R.layout.simple_list_item_2, device_list);
                device_list_view.setAdapter(device_list_adapter);

                scanning_message.setVisibility(View.VISIBLE);
                mBtHandler.send_command(Box_Commands.LIST_PAIRED, channel_to_use);
            }
        });

        device_list = new ArrayList<>();

        device_list_view.setOnItemClickListener(BT_Scan_Activity.this);

    }

    @Override
    protected void onStart() {
        super.onStart();

        mBtHandler.setOnDataReceivedListener(new BluetoothHandler.OnDataReceivedListener() {
            @Override
            public void onDataReceived(byte[] data, int num_bytes) {
                byte command = (byte)(data[0] & 0xFE);
                byte channel = (byte)(data[0] & 0x01);
                Log.d("Command", "Command received." + num_bytes);

                switch(command){
                    case Box_Commands.LIST_PAIRED:
                        /*
                        Handle speaker device data received from the box.

                        Data format: {Command, device address, device name}
                        */

                        byte[] device_addr;
                        byte[] device_name;

                        /*
                        Grab the address and name fields from the raw byte arrays.
                         */
                        device_addr = Arrays.copyOfRange(data, 1, 13);
                        device_name = Arrays.copyOfRange(data, 13, num_bytes);

                        /*
                        Add the new device to the list.
                         */
                        device_list.add(new BT_Device(new String(device_name), new String(device_addr)));
                        device_list_adapter = new BT_Device_List_Adapter(BT_Scan_Activity.this, android.R.layout.simple_list_item_2, device_list);
                        device_list_view.setAdapter(device_list_adapter);

                        scanning_message.setVisibility(View.GONE);

                        break;
                }
            }
        });

        scanning_message.setVisibility(View.VISIBLE);

        /*
        Request device list from box.
         */
        mBtHandler.send_command(Box_Commands.LIST_PAIRED, channel_to_use);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        BT_Device device = device_list.get(i);

        /*
        TODO: If serial communication with transmitter starts working, send command to pair with the selected speaker.

        For now, just make a toast of the name of the selected device.
         */
        Toast.makeText(BT_Scan_Activity.this, device.name, Toast.LENGTH_SHORT).show();
    }
}