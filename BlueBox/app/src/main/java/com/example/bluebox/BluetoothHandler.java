/*
BluetoothHandler.java handles the Bluetooth connection between the phone and box.

Author: Liam Helfrich (whn7g@mst.edu)

Interfaces:
Box_Commands -- Defines constants for the different commands for the box.

Classes:
BluetoothHandler -- A singleton that handles the Bluetooth connection.

NOTE: This Bluetooth code is adapted from Google's official BluetoothChat example application:
https://github.com/android/connectivity-samples/tree/master/BluetoothChat/Application/src/main

And was also inspired by Mitch Tabian's example Bluetooth code:
https://github.com/mitchtabian/Sending-and-Receiving-Data-with-Bluetooth/tree/master/Bluetooth-Communication
 */

package com.example.bluebox;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/*
IDs for commands sent to the box.
 */
interface Box_Commands {
    byte SET_VOL     = 0x00;
    byte MUTE        = 0x02;
    byte UNMUTE      = 0x04;
    byte CONNECT     = 0x06;
    byte DISCONN     = 0x08;
    byte CONN_STATUS = 0x0A;
    byte SCAN        = 0x0C;
    byte PAIR        = 0x0E;
    byte UNPAIR      = 0x10;
    byte LIST_PAIRED = 0x12;
    byte CHN_STATUS  = 0x14;

    byte CHANNEL_A = 0x00;
    byte CHANNEL_B = 0x01;
}

public class BluetoothHandler {
    /*
    This class manages the Bluetooth connection between
    the phone and the box.

    This class is a singleton; all threads/classes share a single instance.
    The instance should be retrieved using the static method getInstance().
     */
    private static volatile BluetoothHandler INSTANCE = null; //Singleton class instance

    public enum ConnectionStatus{
        NOT_CONNECTED,
        CONNECTING,
        CONNECTED,
        CONNECTION_FAILED,
        CONNECTION_LOST
    }
    private ConnectionStatus mStatus; //The current status of the connection

    /*
     Interface for handling changes in the connection status
     */
    public interface OnStatusChangedListener{
        public void onConnectionSuccess(); //Executed when a connection is successfully established.
        public void onConnectionLost(); //Executed when the connection is lost.

        public void onConnectionFailed(); //Executed when the connection could not be successfully established.
    }
    private OnStatusChangedListener mStatusChangedListener = null;

   /*
   Interface for handling data received from the box.
    */
    public interface OnDataReceivedListener{
        /*
        onDataReceived:

        Executed when BT data is received from the box.

        data -- A byte array containing the data
        num_bytes -- The number of bytes of data received.
         */
        public void onDataReceived(byte[] data, int num_bytes);

    }
    public OnDataReceivedListener mDataReceivedListener;

    private static final String appName = "BT_Handler";

    /*
    NOTE: The Bluetooth UUID currently used by the app is a fixed value. This is insecure.
    In the future, this should be updated so a secure Bluetooth UUID is retrieved from the
    Android API.
     */
    private static final UUID MY_UUID_INSECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ConnectThread mConnectThread; //Thread that runs when attempting to connect to the box.
    private ConnectedThread mConnectedThread; //Thread that runs while the box is connected.

    private Handler mHandler; //Event handler for handling whenever the user turns Bluetooth on/off

    BluetoothAdapter mBluetoothAdapter; //Object for interfacing with the Android Bluetooth API
    Context mContext;

    /*
    Class methods
    */
    private BluetoothHandler(Context context) {
        mContext = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mStatus = ConnectionStatus.NOT_CONNECTED;
        mHandler = new Handler();
    }

    public static BluetoothHandler getInstance(Context context){
        /*Retrieves the singleton instance of this class.
        Creates one if one does not yet exist.

        Arguments:
        context -- The application context

        Returns:
        A reference to the single shared instance of this class

        Side effects:
        If BluetoothHandler.INSTANCE is null, a new BluetoothHandler object is instantiated
        and set as the value.

        */
        synchronized (BluetoothHandler.class){
            if (INSTANCE == null) {
                INSTANCE = new BluetoothHandler(context);
            }
        }
        return INSTANCE;
    }

    public void setOnStatusChangedListener(OnStatusChangedListener listener){
        /*Sets the connection status changed listener for this instance.

        Arguments:
        listener -- The listener to set

        Returns: None

        Side effects: The old status changed listener is unregistered and will no longer
                      respond to updates.
        */
        mStatusChangedListener = listener;
    }

    public void setOnDataReceivedListener(OnDataReceivedListener listener){
        /*Sets the data received listener for this instance.

        Arguments:
        listener -- The listener to set

        Returns: None

        Side effects: The old status listener is unregistered and will no longer
                      respond to updates.
        */
        mDataReceivedListener = listener;
    }

    private class ConnectThread extends Thread {
        /*
        This thread runs while trying to establish a Bluetooth connection.
        */
        private final String TAG = "ConnectThread";

        private BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                /*
                Check for bluetooth connection
                 */
                if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Could not establish connection: Bluetooth connect permission not granted.");
                    mStatus = ConnectionStatus.CONNECTION_FAILED;
                    if(mStatusChangedListener != null){
                        mStatusChangedListener.onConnectionFailed(); //Signal a failed connection
                    }
                    return;
                }

                /*
                Create a new Bluetooth socket
                 */
                tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);

            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
                mStatus = ConnectionStatus.CONNECTION_FAILED;
                if(mStatusChangedListener != null){
                    mStatusChangedListener.onConnectionFailed();
                }
            }
            mmSocket = tmp;
        }

        public void run() {
            try {
                /*
                Check for Bluetooth permission
                */
                if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Could not connect to socket: Bluetooth connect permission not granted.");
                    mStatus = ConnectionStatus.CONNECTION_FAILED;
                    if(mStatusChangedListener != null){
                        mStatusChangedListener.onConnectionFailed();
                    }
                    return;
                }

                /*
                Establish a Bluetooth connection
                */
                mmSocket.connect();

            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    Log.e(TAG, "Unable to connect...", connectException);
                    mStatus = ConnectionStatus.CONNECTION_FAILED;
                    if(mStatusChangedListener != null){
                        mStatusChangedListener.onConnectionFailed(); //Signal failed connection
                    }
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // Start the thread to manage the connection and perform transmissions
            Log.d(TAG, "Starting connect thread...");
            mConnectedThread = new ConnectedThread(mmSocket);
            mConnectedThread.start();
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }

    private class ConnectedThread extends Thread {
        /*
        This thread runs while the connection is open.
         */
        private final String TAG = "ConnectedThread";
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            mStatus = ConnectionStatus.CONNECTED;
            if(mStatusChangedListener != null){
                mStatusChangedListener.onConnectionSuccess(); //Signal a successful connection
            }

            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // Read from the InputStream.
                    numBytes = mmInStream.read(mmBuffer);

                    int finalNumBytes = numBytes;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mDataReceivedListener.onDataReceived(mmBuffer, finalNumBytes); //Pass received data on to the data received event listener
                        }
                    });

                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    mStatus = ConnectionStatus.CONNECTION_LOST;
                    if(mStatusChangedListener != null){
                        mStatusChangedListener.onConnectionLost();
                    }
                    break;
                }
            }

        }

        // Call this from the main activity to send data to the remote device.
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);

            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);

            }
        }

        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                mmSocket.close();
                mStatus = ConnectionStatus.NOT_CONNECTED;
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }


    }

    public synchronized void connect(BluetoothDevice device) {
        /* Connect to the provided Bluetooth device.

        Arguments:
        device -- The Bluetooth device to connect to

        Returns: None

        Side effects: Connection state is updated to reflect connection status.
        The relevant event listeners are also called.
         */
        Log.d("Connecting", "connect to: " + device);
        mStatus = ConnectionStatus.CONNECTING;

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();

    }

    public ConnectionStatus get_connection_status(){
        /*
        Getter for the connection status
         */
        return mStatus;
    }

    public void write(byte[] bytes){
        /* Write data to the connected device.

        Arguments:
        bytes -- The bytes to write

        Returns: None

        Side-effects: None
         */
        if(mConnectedThread != null && mConnectedThread.isAlive()){
            mConnectedThread.write(bytes);
        }
        else{
            Log.d("Connecting", "Cannot write; connected thread is dead.");
        }
    }

    public void send_command(byte command, byte channel){
        /* Send a specified command to the connected box with no additional arguments.

        Arguments:
        command -- A valid command ID from the Box_Commands interface

        channel -- Box_Commands.CHANNEL_A or Box_Commands.CHANNEL_B
         */
        write(new byte[]{(byte)(command | channel)});
    }

    public void send_command(byte command, byte channel, byte datum){
        /* Send a specified command to the connected box with one argument data byte.

        Arguments:
        command -- A valid command ID from the Box_Commands interface

        channel -- Box_Commands.CHANNEL_A or Box_Commands.CHANNEL_B

        datum -- The argument byte
         */
        write(new byte[]{(byte)(command | channel), datum});
    }
    public void send_command(byte command, byte channel, byte[] data){
        /* Send a specified command to the connected box with multiple argument bytes

        Arguments:
        command -- A valid command ID from the Box_Commands interface

        channel -- Box_Commands.CHANNEL_A or Box_Commands.CHANNEL_B

        data -- The argument bytes
         */
        write(new byte[]{(byte)(command | channel)});
        write(data);
    }
}
