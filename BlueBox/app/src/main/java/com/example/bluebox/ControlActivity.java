/*
ControlActivity.java defines the behavior for the screen where the user can send commands to the box.
This include volume sliders, mute/unmute buttons, and Bluetooth "pair" buttons.

Author: Liam Helfrich (whn7g@mst.edu)
 */

package com.example.bluebox;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.bluebox.BluetoothHandler;

public class ControlActivity extends AppCompatActivity {
    private BluetoothHandler mBtHandler;
    private Handler mHandler;

    /*
    View elements
     */
    private SeekBar seekBar_ChannelA_Vol;
    private TextView text_ChannelA_Vol;
    private TextView text_ChannelA_Status;
    private Button button_ChannelA_Mute;
    private Button button_ChannelA_Pair;

    private SeekBar seekBar_ChannelB_Vol;
    private TextView text_ChannelB_Vol;
    private TextView text_ChannelB_Status;
    private Button button_ChannelB_Mute;
    private Button button_ChannelB_Pair;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        mBtHandler = BluetoothHandler.getInstance(getApplicationContext());
        mHandler = new Handler();

        /*
        Get view elements
         */
        seekBar_ChannelA_Vol = findViewById(R.id.seekBar_ChannelA_Vol);
        text_ChannelA_Vol    = findViewById(R.id.text_ChannelA_Vol);
        text_ChannelA_Status = findViewById(R.id.text_ChannelA_Status);
        button_ChannelA_Mute = findViewById(R.id.button_ChannelA_Mute);
        button_ChannelA_Pair = findViewById(R.id.button_ChannelA_Pair);

        seekBar_ChannelB_Vol = findViewById(R.id.seekBar_ChannelB_Vol);
        text_ChannelB_Vol    = findViewById(R.id.text_ChannelB_Vol);
        text_ChannelB_Status = findViewById(R.id.text_ChannelB_Status);
        button_ChannelB_Mute = findViewById(R.id.button_ChannelB_Mute);
        button_ChannelB_Pair = findViewById(R.id.button_ChannelB_Pair);

        /*
        View element listeners
         */


        /*
         Channel A volume slider event listeners:
         */
        seekBar_ChannelA_Vol.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                /*
                Update the volume label to show the current volume percentage.
                If the user initiated this change, send the volume change command to the box.
                */
                text_ChannelA_Vol.setText(String.valueOf((int)java.lang.Math.floor(0.5 + 100*i/255.0)) + "%");
                if(fromUser){
                    mBtHandler.send_command(Box_Commands.SET_VOL, Box_Commands.CHANNEL_A, (byte)i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        /*
        Channel A mute button event listeners:
         */
        button_ChannelA_Mute.setOnClickListener(new View.OnClickListener() {
            boolean muted = false;
            @Override
            public void onClick(View view) {
                if(muted){
                    mBtHandler.send_command(Box_Commands.UNMUTE, Box_Commands.CHANNEL_A);
                    button_ChannelA_Mute.setText("MUTE");
                }else{
                    mBtHandler.send_command(Box_Commands.MUTE, Box_Commands.CHANNEL_A);
                    button_ChannelA_Mute.setText("UNMUTE");
                }
                muted = !muted;
            }
        });

        /*
        On click event listener for the Channel A status indicator.
        This view element is currently not visible on the main screen because the corresponding
        functionality could not be implemented in the embedded software.
         */
        text_ChannelA_Status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Launch the Scan Activity to scan for available speakers to pair with:
                 */
                Intent launch_connect_screen = new Intent(ControlActivity.this, BT_Scan_Activity.class);
                launch_connect_screen.putExtra("channel", Box_Commands.CHANNEL_A);
                ControlActivity.this.startActivity(launch_connect_screen);

                mBtHandler.send_command(Box_Commands.LIST_PAIRED, Box_Commands.CHANNEL_A);
            }
        });

        /*
        Temporary fix to the problem listed above:
        The "Pair" button tells the transmitter to pair to the next available speaker.
         */
        button_ChannelA_Pair.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                /*
                Command the Channel A transmitter to pair with the next available speaker.
                 */
                mBtHandler.send_command(Box_Commands.CONNECT, Box_Commands.CHANNEL_A);
            }
        });

        /*
        Channel B volume slider event listeners
         */
        seekBar_ChannelB_Vol.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                /*
                Update the volume label to show the current volume percentage.
                If the user initiated this change, send the volume change command to the box.
                */
                text_ChannelB_Vol.setText(String.valueOf((int)java.lang.Math.floor(0.5 + 100*i/255.0)) + "%");
                if(fromUser){
                    mBtHandler.send_command(Box_Commands.SET_VOL, Box_Commands.CHANNEL_B, (byte)i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        /*
        Channel B mute button event listeners
         */
        button_ChannelB_Mute.setOnClickListener(new View.OnClickListener() {
            boolean muted = false;
            @Override
            public void onClick(View view) {
                if(muted){
                    mBtHandler.send_command(Box_Commands.UNMUTE, Box_Commands.CHANNEL_B);
                    button_ChannelB_Mute.setText("MUTE");
                }else{
                    mBtHandler.send_command(Box_Commands.MUTE, Box_Commands.CHANNEL_B);
                    button_ChannelB_Mute.setText("UNMUTE");
                }
                muted = !muted;
            }
        });

        /*
        On click event listener for the Channel B status indicator.
        This view element is currently not visible on the main screen because the corresponding
        functionality could not be implemented in the embedded software.
         */
        text_ChannelB_Status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Launch the Scan Activity to scan for available speakers to pair with:
                 */
                Intent launch_connect_screen = new Intent(ControlActivity.this, BT_Scan_Activity.class);
                launch_connect_screen.putExtra("channel", Box_Commands.CHANNEL_B);
                ControlActivity.this.startActivity(launch_connect_screen);

                mBtHandler.send_command(Box_Commands.LIST_PAIRED, Box_Commands.CHANNEL_B);
            }
        });

        /*
        Temporary fix to the problem listed above:
        The "Pair" button tells the transmitter to pair to the next available speaker.
         */
        button_ChannelB_Pair.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                /*
                Command the Channel B transmitter to pair with the next available speaker.
                 */
                mBtHandler.send_command(Box_Commands.CONNECT, Box_Commands.CHANNEL_B);
            }
        });

        /*
        Configure an event listener to respond to changes in BT connection with the box:
         */
        mBtHandler.setOnStatusChangedListener(new BluetoothHandler.OnStatusChangedListener() {
            @Override
            public void onConnectionSuccess() {

            }

            @Override
            public void onConnectionLost() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        /*
                        If the connection is lost, launch the Connect screen again with
                        a "connection lost" error message.
                         */
                        Intent connect_window_intent = new Intent(ControlActivity.this, MainActivity.class);
                        connect_window_intent.putExtra("Reason", 1); //CHANGE THIS.
                        ControlActivity.this.startActivity(connect_window_intent);
                        ControlActivity.this.finish();
                    }
                });
            }

            @Override
            public void onConnectionFailed() {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        /*
        Disable the status indicators, as the corresponding functionality could not be
        implemented in the embedded code:
         */
        text_ChannelA_Status.setVisibility(View.GONE);
        text_ChannelB_Status.setVisibility(View.GONE);

        /*
        Configure event listener for receiving data from the box:
         */
        mBtHandler.setOnDataReceivedListener(new BluetoothHandler.OnDataReceivedListener() {
            @Override
            public void onDataReceived(byte[] data, int num_bytes) {
                /*
                Separate the received data into command and channel information:
                 */
                byte command = (byte)(data[0] & 0xFE);
                byte channel = (byte)(data[0] & 0x01);
                Log.d("Command", "Command received." + num_bytes);

                switch(command) {
                    case Box_Commands.CHN_STATUS:
                        Log.d("Command", "Received channel status command.");
                        if (!(num_bytes < 5)) {
                            /*
                            Message format is {Command (0x14), Ch1_Status, Ch1_Vol, Ch2_Status, Ch2_Vol}
                             */

                            /*
                            Vol byte is cast to an int. If Vol > 127, (int)Vol < 0
                            ⇒ (int)Vol & 0xFF is the unsigned value of vol

                            Java is lame for not having unsigned types. >:|

                            Update volume sliders:
                             */
                            seekBar_ChannelA_Vol.setProgress(data[2] & 0xFF);
                            seekBar_ChannelB_Vol.setProgress(data[4] & 0xFF);

                            //Get muted status from channel status bytes, update mute button
                            if ( (data[1] & (1 << 1)) != 0 ){
                                button_ChannelA_Mute.setText("UNMUTE");
                            }else{
                                button_ChannelA_Mute.setText("MUTE");
                            }

                            if ( (data[3] & (1 << 1)) != 0 ){
                                button_ChannelB_Mute.setText("UNMUTE");
                            }else{
                                button_ChannelB_Mute.setText("MUTE");
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        });

        /*
        When the control activity starts, request the channel status data from the box.
        Currently, a request for one channel will return the statuses of both channels, so only
        calling for Channel A will suffice.
         */
        mBtHandler.send_command(Box_Commands.CHN_STATUS, Box_Commands.CHANNEL_A);
    }
}