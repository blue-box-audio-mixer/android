/*
MainActivity.java defines the behavior of the initial screen that shows up on launch.
This screen is where the phone attempts to connect to the box via Bluetooth. It shows an
animation when a connection attempt is in progress, otherwise it shows a relevant error message.

Author: Liam Helfrich (whn7g@mst.edu)

*/

package com.example.bluebox;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

 import com.example.bluebox.BluetoothHandler;
 import com.google.android.material.internal.VisibilityAwareImageButton;

import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";

    /*
    View elements
     */
    AnimationDrawable mConnectAnim; //Animation that appears on screen when attempting a connection

    private static final int REQUEST_BT_CONNECT = 1;
    private static final int REQUEST_BT_ENABLE = 2;

    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothHandler mBtHandler; //Handler for Bluetooth state changes
    private Handler mHandler;

    /*
    Activity lifecycle events:
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBtHandler = BluetoothHandler.getInstance(getApplicationContext());
        mHandler = new Handler();

        /*
        UI event listeners
         */
        Button btn_retry_connection = findViewById(R.id.btn_retry_connection);
        btn_retry_connection.setVisibility(View.INVISIBLE);
        btn_retry_connection.setOnClickListener(new View.OnClickListener() {
            /*Retry the Bluetooth connection*/
            @Override
            public void onClick(View view) {
                connectToBox();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        /*
        Register receiver to detect device Bluetooth state changes
        */
        IntentFilter intentFilter_bt_state_changed = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(bt_BroadcastReceiver, intentFilter_bt_state_changed);

        /*
        Grab relevant display elements:
        */
        TextView text_status = findViewById(R.id.text_status);
        Button btn_retry_connection = findViewById(R.id.btn_retry_connection);

        /*
        Set up the connect animation
        */
        findViewById(R.id.connect_anim_view).setBackgroundResource(R.drawable.bt_anim);
        mConnectAnim = (AnimationDrawable)findViewById(R.id.connect_anim_view).getBackground();

        /*
        Set up an event listener to detect changes in the Bluetooth connection status
        */
        mBtHandler.setOnStatusChangedListener(new BluetoothHandler.OnStatusChangedListener() {
            @Override
            public void onConnectionSuccess() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        text_status.setText("Connected!");
                        mConnectAnim.stop();

                        /*
                        Connected; Launch the control activity.
                        */
                        Intent control_activity_intent = new Intent(MainActivity.this, ControlActivity.class);
                        MainActivity.this.startActivity(control_activity_intent);
                        MainActivity.this.finish();
                    }
                });
            }

            @Override
            public void onConnectionLost() {
            }

            @Override
            public void onConnectionFailed() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        /*
                        Failed to find connection, set the relevant error message.
                        */
                        text_status.setText("Connection failed: Could not find BlueBox.");
                        btn_retry_connection.setVisibility(View.VISIBLE);
                        setConnectionImage(false);
                    }
                });
            }

        });

        /*
        Check if this activity was launched because the connection was lost.
        If so, display the relevant message.
         */
        Intent i = getIntent();
        if(i.hasExtra("Reason")){
            ((TextView)findViewById(R.id.text_status)).setText("Connection lost.");
            ((Button)findViewById(R.id.btn_retry_connection)).setVisibility(View.VISIBLE);
            setConnectionImage(false);
        }else{
            connectToBox();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(bt_BroadcastReceiver);
    }

    /*
    Bluetooth events:
     */

    /*
    If the user declines to turn on Bluetooth when asked, display the relevant message:
    */
    ActivityResultLauncher<Intent> mEnableBluetooth_Launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            final int result_code = result.getResultCode();
            Log.d(TAG, "Bluetooth permission result: " + result.toString());
            if(result_code == RESULT_CANCELED){
                ((TextView)findViewById(R.id.text_status)).setText("Cannot connect because Bluetooth is disabled.");
                ((Button)findViewById(R.id.btn_retry_connection)).setVisibility(View.VISIBLE);
                setConnectionImage(false);
            }
        }
    });

    /*
    Listen for when the user toggles Bluetooth on/off:
     */
    private final BroadcastReceiver bt_BroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(action.equals(mBluetoothAdapter.ACTION_STATE_CHANGED)){
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, mBluetoothAdapter.ERROR);

                switch(state){
                    case BluetoothAdapter.STATE_OFF:
                        //TEAR DOWN BLUETOOTH CONNECTIONS?
                        Log.d(TAG, "Bluetooth disabled. Enabling retry button.");
                        ((TextView)findViewById(R.id.text_status)).setText("Cannot connect because Bluetooth is disabled.");
                        ((Button)findViewById(R.id.btn_retry_connection)).setVisibility(View.VISIBLE);
                        setConnectionImage(false);
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "Bluetooth enabled. Attempting to connect.");
                        connectToBox();
                        break;
                }
            }
        }
    };

    /*
    When the user is asked for permission to use Bluetooth, respond to their choice:
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        TextView text_status = findViewById(R.id.text_status);

        switch (requestCode) {
            case REQUEST_BT_CONNECT:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Now that permission has been granted, retry the connection.
                    Log.d(TAG, "Permission granted!");
                    connectToBox();

                } else {
                    Log.d(TAG, "Permission denied...");
                    text_status.setText("Cannot connect because Bluetooth permission is denied. Please grant BT permission and try again.");
                    ((Button)findViewById(R.id.btn_retry_connection)).setVisibility(View.VISIBLE);
                    setConnectionImage(false);
                }
                return;
        }
    }

    /*
    Helper functions
     */

    private void setConnectionImage(boolean connecting){
        /*
        Set the connection animation based on whether or not a connection
        attempt is currently being made.

        If connection is being made, play the animation.
        Otherwise, show the "no bluetooth" graphic.

        Arguments:
        connecting -- Whether or not a connection attempt is being made.
        */
        ImageView connect_anim = findViewById(R.id.connect_anim_view);
        ImageView no_connect_img = findViewById(R.id.no_connect_img_view);
        if(connecting == true){
            no_connect_img.setVisibility(View.GONE);
            connect_anim.setVisibility(View.VISIBLE);
            mConnectAnim.start();
        }else{
            mConnectAnim.stop();
            connect_anim.setVisibility(View.GONE);
            no_connect_img.setVisibility(View.VISIBLE);
        }
    }
    private void connectToBox(){
        /*
        Attempt to connect to the BlueBox.
        */
        TextView text_status = findViewById(R.id.text_status);
        Button btn_retry_connection = findViewById(R.id.btn_retry_connection);


        btn_retry_connection.setVisibility(View.INVISIBLE);
        text_status.setText("Connecting...");
        setConnectionImage(true);

        Log.d(TAG, "Connecting to box...");

        /*
        Check if the device has Bluetooth
         */
        if (mBluetoothAdapter != null) {
            /*
            Request Bluetooth permissions if they have not yet been granted.
             */
            Log.d(TAG, "Device supports Bluetooth. Checking permission...");
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Permissions not yet granted. Asking...");
                requestPermissions(new String[]{Manifest.permission.BLUETOOTH_CONNECT}, REQUEST_BT_CONNECT);
                return;
            }

            /*
            Ensure Bluetooth is enabled
             */
            if (!mBluetoothAdapter.isEnabled()) {
                /*
                Request to enable Bluetooth. Also request the relevant BT permissions if they have not been granted.
                 */
                Log.d(TAG, "Bluetooth is disabled. Asking to enable...");
                Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                mEnableBluetooth_Launcher.launch(enableBTIntent);

            } else{
                /*
                Bluetooth is enabled and permissions have been granted.
                Start searching for a BlueBox.
                 */
                Set<BluetoothDevice> connected_devices = mBluetoothAdapter.getBondedDevices();
                Log.d(TAG, "Searching for ESP32test...");

                boolean device_found = false;
                for(BluetoothDevice device : connected_devices){
                    if(device.getName().equals("BlueBox")){
                        Log.d(TAG, "Connecting to BlueBox...");
                        device_found = true;
                        mBtHandler.connect(device);
                        break;
                    }
                }

                if(!device_found){
                    text_status.setText("Please pair a BlueBox to your phone and try again.");
                    btn_retry_connection.setVisibility(View.VISIBLE);
                    setConnectionImage(false);
                }
            }

        } else{
            //Device does not have Bluetooth.
            Log.d(TAG, "This device does not support Bluetooth.");

            setConnectionImage(false);
            text_status.setText("Cannot connect because this device does not support Bluetooth.");
        }
    }
}