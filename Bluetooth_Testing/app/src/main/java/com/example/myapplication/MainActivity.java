package com.example.bluetooth_testing;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.BluetoothHandler;

import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_BT_CONNECT = 1;

    BluetoothAdapter mBluetoothAdapter;
    BluetoothHandler mBtHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBtHandler = BluetoothHandler.getInstance(getApplicationContext());

        final SeekBar left_bar = (SeekBar)findViewById(R.id.vol_seek_bar_left);
        final SeekBar right_bar = (SeekBar)findViewById(R.id.vol_seek_bar_right);
        left_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d("SeekBar", "Progress changed");
                xmit(new byte[]{(byte)0x12, (byte)i});
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("SeekBar", "Tracking started");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("SeekBar", "Tracking ended");
            }
        });
        right_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d("SeekBar", "Progress changed");
                xmit(new byte[]{(byte)0x13, (byte)i});
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("SeekBar", "Tracking started");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("SeekBar", "Tracking ended");
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_BT_CONNECT:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    Toast.makeText(MainActivity.this, "Bluetooth Permission Granted", Toast.LENGTH_SHORT).show();
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the feature requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                    Toast.makeText(MainActivity.this, "Bluetooth Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
        }
        // Other 'case' lines to check for other
        // permissions this app might request.
    }

    public void poke(View v) {
        TextView t = findViewById(R.id.text);
        if (checkSelfPermission(Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(MainActivity.this, "Permission already granted", Toast.LENGTH_SHORT).show();

            if (mBluetoothAdapter == null) {
                t.setText("This device does not have Bluetooth.");
            } else if (!mBluetoothAdapter.isEnabled()) {
                //Toast.makeText(MainActivity.this, "Disabling", Toast.LENGTH_SHORT).show();
                Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivity(enableBTIntent);
            } else if (mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.disable();
            }
        } else {
            Toast.makeText(MainActivity.this, "Requesting permission", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_CONNECT}, REQUEST_BT_CONNECT);
        }
    }

    public void list_bt(View v) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            //Request permission to connect bluetooth
            Toast.makeText(MainActivity.this, "Requesting permission", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_CONNECT}, REQUEST_BT_CONNECT);
            return;
        }

        Set<BluetoothDevice> connected_devices = mBluetoothAdapter.getBondedDevices();
        Log.d("ConnectedDevice", "Logging connected devices...");
        for(BluetoothDevice device : connected_devices){
            Log.d("ConnectedDevice", "Connected Device: " + device.getName() + ": " + device.getAddress());
        }

    }

    public void connect_bt(View v) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            //Request permission to connect bluetooth
            Toast.makeText(MainActivity.this, "Requesting permission", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_CONNECT}, REQUEST_BT_CONNECT);
            return;
        }

        Set<BluetoothDevice> connected_devices = mBluetoothAdapter.getBondedDevices();
        Log.d("ConnectedDevice", "Logging connected devices...");
        for(BluetoothDevice device : connected_devices){
            if(device.getName().equals("ESP32test")){
                Log.d("Connecting", "Connecting to ESP32test...");
                mBtHandler.connect(device);
                break;
            }
        }
    }

    public void xmit(byte[] bytes){
        mBtHandler.write(bytes);
    }
}